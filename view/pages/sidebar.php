<div class="vertical-menu">

<div data-simplebar class="h-100">

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li>
                <a href="index.php" class="waves-effect">
                    <i class="ti-home"></i><span class="badge badge-pill badge-primary float-right">2</span>
                    <span>Dashboard</span>
                </a>
            </li>
            <?php if($_SESSION['level'] == 'supplier'){ ?>
            <li class="menu-title">Main</li>
                <li>
                    <a href="?pengelolaanlimbah=data_limbah"><i class="mdi mdi-circle"></i> Penyediaan Bahan</a>
                </li>
            <?php }else{ ?>
            <li class="menu-title">DATA MASTER</li>
            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="mdi mdi-account-edit-outline"></i>
                    <span>Akun Pengguna</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="?pengelolaanlimbah=supplier">Akun Supplier</a></li>
                    <li><a href="?pengelolaanlimbah=pegawai_limbah">Akun Staff</a></li>
                    <li><a href="?pengelolaanlimbah=toko" class="waves-effect">Akun Toko</a></li>
                </ul>
            </li>
            <li >
                <a href="?pengelolaanlimbah=data_limbah" class="waves-effect">
                    <i class="mdi mdi-box"></i>
                    <span>Bahan Mentah</span>
                </a>
            </li>
            <li>
                
            </li>
            <li class="menu-title">TRANSAKSI</li>
            <li>
                <a href="?pengelolaanlimbah=data_angkut" class="waves-effect">
                    <i class="mdi mdi-vector-circle"></i>
                    <span>Barang Angkut</span>
                </a>
            </li>
            <li >
                <a href="?pengelolaanlimbah=referensibarang" class="waves-effect">
                    <i class="mdi mdi-book-open-page-variant"></i>
                    <span>Referensi Barang</span>
                </a>
            </li>
            <li>
                <a href="?pengelolaanlimbah=daurulang" class="waves-effect">
                    <i class="mdi mdi-vector-circle"></i>
                    <span>Daur Ulang</span>
                </a>
            </li>
            <li>
                <a href="?pengelolaanlimbah=barang_jadi" class="waves-effect">
                    <i class="mdi mdi-car-pickup"></i>
                    <span>Kirim Barang</span>
                </a>
            </li>
            <li class="menu-title">LAPORAN</li>
            <li>
                <a href="javascript: void(0);" class="has-arrow waves-effect">
                    <i class="mdi mdi-cloud-print"></i>
                    <span>Laporan</span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="?pengelolaanlimbah=laporan_supplier">Supplier</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_toko">Toko</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_data_limbah">Data Limbah</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_data_angkut">Pengadaan Limbah</a></li>
                    <!-- <li class="menu-title">Proses Daur Ulang</li>     -->
                    <li><a href="?pengelolaanlimbah=laporan_referensi">Referensi Barang</a></li>
                    <li><a href="?pengelolaanlimbah=laporan_kirim">Data Kirim</a></li>
                </ul>
            </li>
            <?php } ?>
            <li class="menu-title">LAINNYA</li>
            <li>
                <a href="logout.php" class="waves-effect">
                    <i class="ti-home"></i>
                    <span>LOG OUT</span>
                </a>
            </li>

           



        </ul>
    </div>
    <!-- Sidebar -->
</div>
</div>