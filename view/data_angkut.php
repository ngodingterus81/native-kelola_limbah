<?php error_reporting(0); ?>
<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h3>Data Angkut</h3>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Data Angkut</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h3>Table Data Angkut</h3> 
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <form method="post">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Tanggal Angkut</th>
                                  <?php if($_SESSION['level'] != 'supplier'){ ?>
                                    <th>Supplier</th>
                                  <?php } ?>
                                  <th>Nama Limbah</th>
                                  <th>Jml (Kg)</th>
                                  <th>Harga (1kg)</th>
                                  <th>Total Limbah</th>
                                  <?php if($_SESSION['level']!='supplier'){ ?>
                                    <th>Jumlah Barang</th>
                                  <?php } ?>
                                  <th>act</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $nomor=1;
                                    $cek = mysqli_query($conn, "SELECT * FROM data_limbah dl 
                                    JOIN data_angkut_detail dh ON dl.kd_limbah=dh.kd_limbah 
                                    JOIN data_angkut a ON dh.id_angkut=a.id_angkut 
                                    JOIN login b ON a.kd_supplier=b.id");
                                  ?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr>
                                    <td><?php echo $nomor++; ?></td>
                                    <td><?php echo $row['tgl_angkut']; ?></td>
                                    <?php if($_SESSION['level'] != 'supplier'){ ?>
                                      <td><?php echo $row['username']; ?></td>
                                    <?php } ?>
                                    <td><?php echo $row['namalimbah']; ?></td>
                                    <td><?= $row['jml_kg']; ?> Kg</td>
                                    <td><?php echo 'Rp. '.number_format($row['harga_satuan'], 0,',','.'); ?></td>
                                    <td><?php echo 'Rp. '.number_format($row['total'], 0,',','.');; ?></td>
                                    <?php if($_SESSION['level']!='supplier'){ ?>
                                      <td width="80">
                                        <?php if($row['stts_detail'] == 0){ ?>
                                          <input type="number" id="<?= $row['Id'] ?>_jml" name="jml" style="width: 100%;">
                                        <?php }else{ echo"-"; } ?>
                                      </td>
                                    <?php } ?>
                                    <td>
                                      <?php if($row['stts_detail'] == 0){ ?>
                                        <input type="hidden" id="<?= $row['Id'] ?>_lmb"  value="<?= $row['kd_limbah'] ?>">
                                        <input type="hidden" name="id_angkut" value="<?= $row['id_angkut'] ?>">
                                        <input type="hidden" id="<?= $row['Id'] ?>" name="id" value="<?= $row['Id'] ?>">
                                        <?php if($_SESSION['level']=='supplier'){ ?>
                                          <button class="btn btn-danger" type="button" name="hps" onclick="batal('<?= $row['id_angkut'] ?>', '<?= $row['Id'] ?>')" >Batalkan</button>
                                      <?php }else{ ?>
                                        <button class="btn btn-primary" type="button" onclick="konf(document.getElementById('<?= $row[Id] ?>'), document.getElementById('<?= $row[Id] ?>_jml'),  document.getElementById('<?= $row[Id] ?>_lmb'));" name="knf">Konfirmasi</button>
                                      <?php } ?>
                                      <?php }else{echo"-";} ?>
                                    </td>
                                  </tr>
                    
                  <?php } ?>
                                  </tbody>
                              </table>
                              </form>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>

 <script type="text/javascript">
  function batal(id_angkut, id){
    location.href='view/proses/btl.php?id_angkut='+id_angkut+'&id='+id;
  }

   function konf(id, jml, lmb){
    if (jml == null) {
      alert('Jumalh barang belum ditentukan...');
    }else{
      location.href='view/proses/knf.php?stts=knf&jml='+jml.value+'&kd_limbah='+lmb.value+'&id='+id.value;
    }
   }
 </script>