<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h2>Data Pegawai</h2>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Akun Pengguna</a></li>
                              <li class="breadcrumb-item active">Data Pegawai</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h3 class="card-title"><a href="?pengelolaanlimbah=form_pegawai_limbah"><button type="button" class="btn btn-block btn-primary btn-lg">Add Pegawai Limbah</button></a></h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>Kode Pegawai</th>
                                  <th>Nama Pegawai</th>
                                  <th>Alamat</th>
                                  <th>No Telpon</th>
                                  <th>Email</th>
                                  <th>act</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $nomor=1;
                                  $cek = mysqli_query($conn, "SELECT * FROM login WHERE level='manager'");?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr>
                                    
                                    <td><?php echo $row['kode']; ?></td>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['alamat']; ?></td>
                                    <td><?php echo $row['telepon']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    <td>
                                      <a href="?pengelolaanlimbah=hapus_pegawai_limbah&no=<?php echo $row['id'];?>"><button class="btn-danger btn">Hapus</button></a>

                                      <a href="?pengelolaanlimbah=edit_pegawai_limbah&no=<?php echo $row['id'];?>"><button class="btn-warning btn">Ubah</button></a>
                                    </td>
                                  </tr>
                    <?php $nomor++; ?>
                  <?php } ?>
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>

