
<?php
    $id=$_GET['no'];
    $hasil = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id'");
    $row=mysqli_fetch_array($hasil);
    
?> 


<div class="main-content">

                <div class="page-content">
                  <div class="container-fluid">

                  <!-- ini kontent -->
     <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Kirim Barang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Form Data Limbah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form action="view/proses/proses_send.php" method="POST">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Form Data Limbah</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama Barang</label>
                  <input type="hidden"  value="<?php echo $row['id_barang'];?>" name="id_barang" id="id_barang" class="form-control">
                  <input type="text" disabled="disabled" value="<?php echo $row['nm_barang'];?>" name="nm_barang" id="nm_barang" class="form-control">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                <label>Nama Toko</label>
                <select name="kd_toko" id="kd_toko" class="form-control">
                  <?php
                    $hasill = mysqli_query($conn, "SELECT * FROM toko");
                    while($hsll=mysqli_fetch_array($hasill)){
                  ?>
                  <option value="<?=$hsll['kd_toko']?>"><?=$hsll['namatempat']?></option> 
                    <?php } ?>
                </select>
                </div>
                <div class="form-group">
                  <label>Tanggal</label>
                  <input class="form-control" type="date" name="tanggal" id="tanggal" required="">
                
                </div>
                <div class="form-group">
                  <label>Stok Tersedia</label>
                  <input type="text" name="stok" id="stok" class="form-control" readonly="" value="<?= $row['stok'] ?>">
                </div>
                <div class="form-group">
                  <label>Jumlah</label>
                  <input type="number" max="<?= $row['stok'] ?>" name="jumlah" id="jumlah" class="form-control">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              
              </div>
              
              <div class="card-footer">
                <input type="submit" name="save" value="Simpan" class="btn btn-block btn-primary">
              </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
                  <!-- tutup content -->


                  </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            
            <!-- end main content-->

</div>