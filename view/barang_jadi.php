<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h1>Data Barang</h1>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">kirim Barang</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <form action="view/proses/proses_send.php?send=sends" method="POST">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <div class="row">
                                <select class="form-control col-3" required="" name="toko">
                                  <option value="" hidden="">--Pilih Toko--</option>
                                  <?php foreach (mysqli_query($conn, "SELECT * FROM login where level = 'toko'") as $val) { ?>
                                    <option value="<?= $val['kode'] ?>"><?= $val['nama'] ?></option>
                                  <?php } ?>
                                </select>
                                <input type="date" name="tanggal" class="form-control col-3" required="">
                                &nbsp;&nbsp;&nbsp;
                                <button class="btn btn-primary"><b>Kirim barang</b></button>
                              </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th><center><b><i class="mdi mdi-check"></i></b></center></th>
                                  <th width="150"><center>Jumlah Kirim</center></th>
                                  <th>Kode Barang</th>
                                  <th>Nama Barang</th>
                                  <th><center>Stok</center></th>
                                  <th><center>last update</center></th>
                                  <!-- <th><center>Act</center></th> -->
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $nomor=1;
                                  $cek = mysqli_query($conn, "SELECT * FROM ref_barang");?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr>
                                    <td><?php echo $nomor++; ?></td>
                                    <td align="center"><input type="checkbox" name="id_barang[]" value="<?= $row['id_ref'] ?>"></td>
                                    <td align="center"><input type="number" name="jml[]" class="form-control col-sm-6" max="<?= $row['stok'] ?>" placeholder="0"></td>
                                    <td><?php echo $row['id_ref']; ?></td>
                                    <td><?php echo $row['ref_nama']; ?></td>
                                    <td align="center"><?php echo $row['stok']; ?></td>
                                    <td align="center"><?php echo $row['upt']; ?></td>
                                    <!-- <td>
                                       <button class="btn btn-danger" onclick="location.href='?pengelolaanlimbah=barang_jadi&id=<?= $row['id_barang'] ?>'"><b>Hapus</b></button>
                                    </td> -->
                                  </tr>
                    
                  <?php } ?>
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                      </form>
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>

<?php  
  
if(isset($_GET['id'])){
    if(mysqli_query($conn, "DELETE FROM barang WHERE id_barang = '$_GET[id]'")){
        echo"<script>alert('Berhasil Menghapus referensi....');</script>";
        echo"<script>location.href='?pengelolaanlimbah=barang_jadi';</script>";
    }
}

?>