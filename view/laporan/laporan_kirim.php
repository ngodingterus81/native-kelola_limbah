<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row mb-2">
                          <div class="col-sm-6">
                            <h4>Data Barang</h4>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item active">Kirim Barang</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <div class="row">
                                <button class="hide btn btn-primary" onclick="window.print()"><b><i class="fa fa-print"></i> Cetak</b></button>
                              </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Tgl Kirim</th>
                                  <th>Toko</th>
                                  <th>Kode Barang</th>
                                  <th>Nama Barang</th>
                                  <th><center>Stok</center></th>
                                  <th>Harga</th>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $nomor=1;
                                  $cek = mysqli_query($conn, "SELECT * FROM kirim_barang a JOIN kirim_barang_detail b ON a.id_kirim=b.id_kirim JOIN ref_barang d ON d.id_ref=b.id_barang JOIN login e ON a.kd_toko=e.kode");?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr>
                                    <td><?php echo $nomor++; ?></td>
                                    <td><?php echo $row['tanggal']; ?></td>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['id_barang']; ?></td>
                                    <td><?php echo $row['ref_nama']; ?></td>
                                    <td align="center"><?php echo $row['jumlah']; ?></td>
                                    <td align="center"><?php echo $row['hrga']; ?></td>
                                  </tr>
                    
                  <?php } ?>
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>