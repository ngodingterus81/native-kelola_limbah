<?php $dta = array(); ?>
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b>Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Tgl Pengadaan</center></th>
                                    <th><center>Supplier</center></th>
                                    <th><center>Nama Limbah</center></th>
                                    <th><center>Jumlah Kg</center></th>
                                    <th><center>Harga Satuan</center></th>
                                    <th><center>Total Harga</center></th>
                                </thead>
                                <tbody>
                                	<?php foreach(mysqli_query($conn, "SELECT * FROM data_angkut a JOIN data_angkut_detail c ON a.id_angkut=c.id_angkut JOIN login b ON a.kd_supplier=b.id GROUP BY a.id_angkut") AS $ref){ ?>
                                    <?php foreach(mysqli_query($conn, "SELECT * FROM data_angkut_detail a JOIN data_limbah b ON a.kd_limbah=b.kd_limbah WHERE a.id_angkut = '$ref[id_angkut]'") AS $no => $red) : ?>
                                        <tr>
                                            <td align="center"><?= $ref['tgl_angkut'] ?></td>
                                            <td align="center"><?= $ref['username'] ?></td>
                                            <td><?= $red['namalimbah'] ?></td>
                                            <td align="center"><?= $red['jml_kg'] ?></td>
                                            <td><?= 'Rp. '.number_format($red['harga_satuan'], 0, ',', '.') ?></td>
                                            <td><?= 'Rp. '.number_format($red['total'], 0, ',', '.') ?></td>
                                        </tr>
                                    <?php $dta[$no] = $red['total']; endforeach; ?>
                                    <tr>
                                        <td colspan="5" align="right">Total Keseluruhan</td>
                                        <td><?= 'Rp. '.number_format(array_sum($dta), 0, ',', '.') ?></td>
                                        <?php unset($dta); ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>