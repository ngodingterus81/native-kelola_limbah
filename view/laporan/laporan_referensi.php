<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <button type="button" class="hide btn btn-primary waves-effect waves-light" onclick="window.print()"><i class="fa fa-print"></i> <b> Cetak</b></button>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th><center>#</center></th>
                                    <th><center>Referensi Nama Barang</center></th>
                                    <th><center>Pengerjaan/1pcs</center></th>
                                    <th><center>Bahan - Bahan</center></th>
                                </thead>
                                <tbody>
                                <?php foreach(mysqli_query($conn, "SELECT * FROM ref_barang ORDER BY ref_nama ASC") AS $no => $ref){ ?>
                                    <tr>
                                        <td><?= $no+1 ?></td>
                                        <td><?= $ref['ref_nama'] ?></td>
                                        <td><?= $ref['waktu_pengerjaan'] ?> <?php if($ref['waktu_pengerjaan'] ==30){echo" Menit";}else{echo" Jam";} ?></td>
                                        <td>
                                            <table class="table">
                                                <?php foreach (mysqli_query($conn, "SELECT * FROM ref_barang_det a JOIN data_limbah b USING(kd_limbah) WHERE a.id_ref = '$ref[id_ref]'") as $val) { ?>
                                                    <tr>
                                                        <td><?= $val['namalimbah'] ?></td>
                                                        <td><?= $val['ambil_stok'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>