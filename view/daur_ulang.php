<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

          <!-- ini kontent -->
            <section class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">
                  <div class="col-sm-6">
                    <h3>Proses Daur Ulang</h3>
                  </div>
                  <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                      <li class="breadcrumb-item active">Daur Ulang</li>
                    </ol>
                  </div>
                </div>
              </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">    
                <div class="row">
                    <?php foreach(mysqli_query($conn, "SELECT * FROM proses_daur a JOIN ref_barang b USING(id_ref) WHERE a.status = 1") AS $du): ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                            <!-- <div class="alert alert-warning">
                                <?php 
                                    error_reporting(0);

                                    $nil = $du['waktu_pengerjaan']*$du['jml_buat'];
                                    $end = date('Y/m/d H:i:s', strtotime("+$nil Hour", strtotime($du['tgl_pelaksanaan'], date("Y/m/d H:i:s"))));
                                    $now = date("Y/m/d H:i:s");
                                    $a = date_create($end);
                                    $b = date_create($now);
                                    $c = date_diff($a, $b);
                                    if($now > $du['waktu_pengerjaan']){
                                        echo 'Waktu proses...';
                                        echo $ja = $c->h." Jam ";
                                        echo $mm = $c->i." Menit ";

                                        $e = date_create($end);
                                        $f = date_create($du['tgl_pelaksanaan']);
                                        $g = date_diff($e, $f);

                                        $ab = ($g->h-$c->h)*100/$g->h;
                                        if($ab > 100 || $ab < 0){
                                            $hs = 0;
                                        }else{
                                            $hs = $ab;
                                        }
                                    }else{
                                        echo 'Menunggu waktu pengerjaan...';
                                        $hs = 0;
                                    }

                                ?>

                            </div> -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4 class="card-title"><?= $du['ref_nama'] ?></h4>
                                        <p>Tanggal Proses, <?= substr($du['tgl_pelaksanaan'], 0, 10) ?></p>

                                    </div>
                                    <div class="col-lg-6 text-right">
                                        <p id="mtime" style="display: none;"><?= $du['tgl_pelaksanaan'] ?></p>
                                        <h4 class="card-title">Waktu Pengerjaan &plusmn; <?= $du['waktu_pengerjaan']*$du['jml_buat'] ?> Jam.</h4>
                                        <p>Jumlah Pembuatan <?= $du['jml_buat'] ?> Pcs</p>
                                    </div>
                                </div>

                                <!-- <div class="">
                                    <div class="progress">
                                        <?php $prg = number_format($hs, 0,',', '.'); ?>
                                        <div class="progress-bar" role="progressbar" aria-valuenow="75" style="width: <?= $prg ?>%;" aria-valuemin="0" aria-valuemax="100"><span><?= $prg.' %'; ?></span></div>
                                    </div>
                                </div> -->
                                <p></p>
                                <form action="" method="POST">
                                        <input type="text" name="nama" value="<?= $du['ref_nama'] ?>" readonly hidden>
                                        <input type="text" name="jml" value="<?= $du['jml_buat'] ?>" readonly hidden>
                                        <input type="text" name="ref" value="<?= $du['id_ref'] ?>" readonly hidden>
                                        <input type="text" name="id_ref" value="<?= $du['Id'] ?>" readonly hidden>
                                        <?php //if($hs == 100){ ?>
                                            <button type="submit" name="simpan" class="btn btn-success">Selesai</button>
                                        <?php //}else{ ?>
                                        <!-- <button type="button" onclick="tolak('<?= $du['ref_nama'] ?>')" class="btn btn-warning">Selesai</button> -->
                                        <?php //} ?>
                                        <!-- <script type="text/javascript">
                                            function tolak(param){
                                                alert('maaf '+param+' masih dalam tahap pengerjaan');
                                            }
                                        </script> -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </section>
        </div>
    </div>
</div>

<?php  
    if(isset($_POST['simpan'])){
        if(mysqli_query($conn, "UPDATE proses_daur SET status = 2 WHERE Id='$_POST[id_ref]'")){

            $stl = mysqli_query($conn, "SELECT * FROM ref_barang a JOIN ref_barang_det b USING(id_ref) JOIN data_limbah c USING(kd_limbah) WHERE a.id_ref='$_POST[ref]'");
            while($dtaa = mysqli_fetch_assoc($stl)){
                mysqli_query($conn, "UPDATE data_limbah SET stok = stok - ($dtaa[ambil_stok]*$_POST[jml]) WHERE kd_limbah='$dtaa[kd_limbah]'");
            }

            mysqli_query($conn, "UPDATE ref_barang SET stok = stok + $_POST[jml] WHERE id_ref='$_POST[ref]'");
            
        }
        echo "<script>location.href='?pengelolaanlimbah=referensibarang'</script>";
    }

?>