<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                  <!-- ini kontent -->
                    <section class="content-header">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-sm-6">
                            <h2>Bahan Mentah</h2>
                          </div>
                          <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                              <li class="breadcrumb-item"><a href="#">Data Limbah</a></li>
                              <li class="breadcrumb-item active">Bahan Mentah</li>
                            </ol>
                          </div>
                        </div>
                      </div><!-- /.container-fluid -->
                    </section>

                    <!-- Main content -->
                    <section class="content">
                      <!-- <form class="row" action="view/proses/proses_pengajuan.php" method="POST" enctype="multipart-form/data"> -->
                        <form class="row" action="view/proses/proses_pengajuan.php" method="POST" enctype="multipart-form/data">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header">
                              <h3 class="card-title">
                                <?php if($_SESSION['level'] != 'supplier'){ ?>
                                  <a href="?pengelolaanlimbah=form_data_limbah">
                                    <button type="button" class="btn btn-block btn-primary btn-lg">Add Data Limbah</button>
                                  </a>
                                <?php }else{ ?>
                                  <a href="?pengelolaanlimbah=data_angkut">
                                    <button type="button" class="btn btn-block btn-primary btn-lg">Lilhat Data Angkut</button>
                                  </a>
                                <?php } ?>
                              </h3>
                                <?php if($_SESSION['level'] == 'supplier'){ ?>
                                  <h3 class="card-title">
                                  <div class="row">
                                  <input type="date" class="col-3 form-control" name="tgl" required="">
                                  &nbsp;&nbsp;&nbsp;
                                  <button class="btn btn-success">Mengajukan</button>
                                  </div>
                                </h3>
                                <?php } ?>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                              <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <?php if($_SESSION['level'] == 'supplier'){ ?>
                                    <th>
                                      <center>
                                        <i class="mdi mdi-check"></i>  
                                      </center>
                                    </th>
                                    <th>
                                      <center>
                                        Jumlah Kg
                                      </center>
                                    </th>
                                  <?php } ?>
                                  <th>Kode Limbah</th>
                                  <th>Nama Limbah</th>
                                  <?php if($_SESSION['level'] != 'supplier'){ ?>
                                    <th>Stok</th>
                                    <th>Penambahan Stok Terakhir</th>
                                  <?php } ?>
                                  <th>Harga(1kg)</th>
                                  <?php if($_SESSION['level'] != 'supplier'){ ?>
                                    <th><center>Act</center></th>
                                  <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                  <?php $nomor=1;
                                  $cek = mysqli_query($conn, "SELECT * FROM data_limbah");?>
                                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                                  <tr> 
                                    <td><?php echo $nomor; ?></td>

                                    <?php if($_SESSION['level'] == 'supplier'){ ?>
                                      <td align="center">
                                        <input type="checkbox" name="id_limbah[]" value="<?= $row['kd_limbah'] ?>">
                                      </td>
                                      <td width="100">
                                        <input type="number" name="kg[]" style="width:100%;">
                                      </td>
                                    <?php } ?>
                                    <td><?php echo $row['kd_limbah']; ?></td>
                                    <td><?php echo $row['namalimbah']; ?></td>
                                    <?php if($_SESSION['level'] != 'supplier'){ ?>
                                      <td><?php echo $row['stok']; ?></td> 
                                      <td>
                                       <?php echo $row['upt']; ?>   
                                     </td> 
                                    <?php } ?>
                                    <td>
                                      <?php
                                        echo 'Rp. '.number_format($row['harga'], 0,',','.');
                                      ?>                                      
                                    </td>
                                  <?php if($_SESSION['level'] != 'supplier'){ ?>
                                    <td> <a
                                    href="?pengelolaanlimbah=hapus_data_limbah&no=<?php
                                    echo $row['id'];?>"
                                    class="btn-danger btn">Hapus</a>

                                        <a href="?pengelolaanlimbah=edit_data_limbah&no=<?php echo $row['id'];?>" class="btn-warning btn">Ubah</a>
                                    </td>
                                  <?php } ?>
                                  </tr>
                    <?php $nomor++; ?>
                  <?php } ?>
                                  </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </form>
                      <!-- /.row -->
                    </section>
                  <!-- tutup content -->


                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


                
               <!-- ini footer -->
            </div>
            <!-- end main content-->

        </div>

