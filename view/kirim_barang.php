<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <a href="?pengelolaanlimbah=barang_jadi"><button type="button" class="hide btn btn-primary waves-effect waves-light"> <b>Cek Barang</b></button></a>
                        </div>
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th width="20"><center>Kode Barang</center></th>
                                    <th><center>Nama Barang</center></th>
                                    <th><center>Stok Tersedia</center></th>
                                </thead>
                                <tbody>
                                	<?php $query = "SELECT * FROM ref_barang" ?>
                                <?php foreach(mysqli_query($conn, $query) AS $no => $ref){ ?>
                                    <tr>
                                        <td align="center"><?= $ref['id_ref'] ?></td>
                                        <td align="center"><?= $ref['ref_nama'] ?></td>
                                        <td align="center"><?= $ref['stok'] ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>