﻿# Host: localhost  (Version 5.5.5-10.1.30-MariaDB)
# Date: 2020-04-28 02:38:47
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "data_angkut"
#

DROP TABLE IF EXISTS `data_angkut`;
CREATE TABLE `data_angkut` (
  `id_angkut` varchar(20) NOT NULL DEFAULT '',
  `tgl_angkut` date DEFAULT NULL,
  `kd_supplier` varchar(20) DEFAULT NULL,
  `status_angkut` int(1) DEFAULT '0' COMMENT '0:pengajuan, 1:angkut',
  PRIMARY KEY (`id_angkut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "data_angkut"
#

INSERT INTO `data_angkut` VALUES ('AK0001','2020-04-24','4',0),('AK0002','2020-04-24','4',0),('AK0003','2020-04-26','4',0),('AK0004','2020-04-28','4',0),('AK0005','2020-04-30','4',0),('AK0006','2020-04-28','4',0),('AK0007','2020-04-28','4',0),('AK0008','2020-04-28','4',0),('AK0009','2020-04-30','4',0),('AK0010','2020-04-28','4',0),('AK0011','2020-04-29','4',0),('AK0012','2020-04-30','4',0),('AK0013','2020-04-29','4',0),('AK0014','2020-04-23','4',0),('AK0015','2020-04-23','4',0),('AK0016','2020-04-30','4',0);

#
# Structure for table "data_angkut_detail"
#

DROP TABLE IF EXISTS `data_angkut_detail`;
CREATE TABLE `data_angkut_detail` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `id_angkut` varchar(20) DEFAULT NULL,
  `kd_limbah` varchar(20) DEFAULT NULL,
  `jml_kg` int(11) DEFAULT NULL,
  `harga_satuan` int(255) DEFAULT NULL,
  `total` int(255) DEFAULT NULL,
  `stts_detail` int(11) DEFAULT '0' COMMENT '0:wait, 1;success',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

#
# Data for table "data_angkut_detail"
#

INSERT INTO `data_angkut_detail` VALUES (1,'AK0001','KL0001',2,1000,2000,1),(2,'AK0002','KL0001',2,1000,2000,1),(3,'AK0003','KL0001',4,1000,4000,1),(4,'AK0004','KL0001',20,1000,20000,0),(5,'AK0004','KL0002',10,1000,10000,1),(6,'AK0004','KL0003',50,500,25000,0),(8,'AK0008','KL0001',10,1000,10000,0),(13,'AK0011','KL0003',10,500,5000,0),(19,'AK0015','KL0002',2,1000,2000,0),(20,'AK0015','KL0003',10,500,5000,1),(21,'AK0016','KL0002',2,1000,2000,0);

#
# Structure for table "data_limbah"
#

DROP TABLE IF EXISTS `data_limbah`;
CREATE TABLE `data_limbah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_limbah` varchar(10) NOT NULL DEFAULT 'KL0001',
  `namalimbah` varchar(20) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `tgl_penambahan` date DEFAULT NULL,
  `harga` varchar(20) DEFAULT NULL,
  `upt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "data_limbah"
#

INSERT INTO `data_limbah` VALUES (1,'KL0001','Plastik',18,'0000-00-00','1000','2020-04-28 02:38:07'),(2,'KL0002','Kardus',34,'0000-00-00','1000','2020-04-28 02:38:07'),(3,'KL0003','Botol Aqua',30,'0000-00-00','500','2020-04-28 02:38:07');

#
# Structure for table "kirim_barang"
#

DROP TABLE IF EXISTS `kirim_barang`;
CREATE TABLE `kirim_barang` (
  `id_kirim` char(10) NOT NULL DEFAULT '',
  `kd_toko` char(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id_kirim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "kirim_barang"
#

INSERT INTO `kirim_barang` VALUES ('KRM0001','KT0001','2020-04-28');

#
# Structure for table "kirim_barang_detail"
#

DROP TABLE IF EXISTS `kirim_barang_detail`;
CREATE TABLE `kirim_barang_detail` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kirim` varchar(20) DEFAULT NULL,
  `id_barang` varchar(20) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `key_send` (`id_kirim`),
  CONSTRAINT `key_send` FOREIGN KEY (`id_kirim`) REFERENCES `kirim_barang` (`id_kirim`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "kirim_barang_detail"
#

INSERT INTO `kirim_barang_detail` VALUES (1,'KRM0001','BR0001',3);

#
# Structure for table "login"
#

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(8) DEFAULT '-',
  `nama` varchar(100) DEFAULT '-',
  `alamat` varchar(150) DEFAULT '-',
  `telepon` varchar(15) DEFAULT '-',
  `email` varchar(80) DEFAULT '-',
  `username` varchar(15) NOT NULL,
  `password` varchar(150) NOT NULL DEFAULT '',
  `level` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

#
# Data for table "login"
#

INSERT INTO `login` VALUES (1,'KP0001','admin','admin','-','admin@admin.com','admin','admin','manager'),(4,'SP0001','PT Indah Pustaka Sejahtera','Karawaci - Cimone','0213','pustakasejahtera@gmail.com','SP0001','123','supplier'),(6,'KT0001','Toko Mami Isti','Cimone','0921','mmisti@gmail.com','KT0001','321','toko');

#
# Structure for table "proses_daur"
#

DROP TABLE IF EXISTS `proses_daur`;
CREATE TABLE `proses_daur` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pelaksanaan` datetime DEFAULT NULL,
  `jml_buat` int(4) DEFAULT NULL,
  `id_ref` varchar(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 proses, 2 selesai, 3 berhasil, 4 gagal',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "proses_daur"
#

INSERT INTO `proses_daur` VALUES (2,'2020-04-24 21:18:27',5,'BR0001',2),(3,'2020-04-28 20:15:12',1,'BR0001',2),(4,'2020-04-29 21:50:28',3,'BR0001',2),(6,'2020-04-29 02:35:50',2,'BR0002',2);

#
# Structure for table "ref_barang"
#

DROP TABLE IF EXISTS `ref_barang`;
CREATE TABLE `ref_barang` (
  `id_ref` varchar(10) NOT NULL DEFAULT '',
  `ref_nama` varchar(50) DEFAULT NULL,
  `hrga` int(11) DEFAULT NULL,
  `waktu_pengerjaan` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT '0',
  `upt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "ref_barang"
#

INSERT INTO `ref_barang` VALUES ('BR0001','a',2222,1,9,'2020-04-28 02:37:50'),('BR0002','b',20000,2,2,'2020-04-28 02:38:07');

#
# Structure for table "ref_barang_det"
#

DROP TABLE IF EXISTS `ref_barang_det`;
CREATE TABLE `ref_barang_det` (
  `id_ref_det` int(11) NOT NULL AUTO_INCREMENT,
  `kd_limbah` varchar(10) DEFAULT NULL,
  `ambil_stok` int(11) DEFAULT NULL,
  `id_ref` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_ref_det`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "ref_barang_det"
#

INSERT INTO `ref_barang_det` VALUES (1,'KL0001',10,'BR0001'),(2,'KL0003',10,'BR0002'),(3,'KL0002',3,'BR0002'),(4,'KL0001',6,'BR0002');
